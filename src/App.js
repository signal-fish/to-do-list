import React, { Component } from 'react'
import Header from './components/Header'
import List from './components/List'
import Footer from './components/Footer'
import './App.css'

export default class App extends Component {
  // initialize state
  state = {
    todos: [
      { id: '001', name: 'Eat', done: true },
      { id: '002', name: 'Code', done: true },
      { id: '003', name: 'Sleep', done: false },
    ]
  }

  // used to add a new todo object
  addTodo = (todoObj) => {
    // Get the original todos
    const { todos } = this.state;
    // prepend a new todo object
    const newTodos = [todoObj, ...todos];
    // Update state
    this.setState({ todos: newTodos })
  }

  // used to update a todo object
  updateTodo = (id, done) => {
    // Get the todos array
    const { todos } = this.state
    // Process todos array
    const newTodos = todos.map((todoObj) => {
      if (todoObj.id === id) return { ...todoObj, done }
      else return todoObj
    })
    // Update state
    this.setState({ todos: newTodos })
  }

  // used to delete a todo object
  deleteTodo = (id) => {
    // Get the original todos
    const { todos } = this.state
    // Delete the specified todo object
    const newTodos = todos.filter((todoObj) => {
      return todoObj.id !== id;
    })
    // Update state
    this.setState({ todos: newTodos })
  }

  // used to check all todo objects
  checkAllTodo = (done) => {
    // Get the original todos
    const { todos } = this.state
    // Process data
    const newTodos = todos.map((todoObj) => {
      return { ...todoObj, done }
    })
    // Update state
    this.setState({ todos: newTodos })
  }

  // used to clear all finished todo objects
  clearAllDone = () => {
    // Get the original todos
    const { todos } = this.state
    // Process data
    const newTodos = todos.filter((todoObj) => {
      return !todoObj.done
    })
    // Update state
    this.setState({ todos: newTodos })
  }

  render() {
    const { todos } = this.state;
    return (
      <div className="todo-container">
        <div className="todo-wrap">
          <Header addTodo={this.addTodo} />
          <List todos={todos} updateTodo={this.updateTodo} deleteTodo={this.deleteTodo} />
          <Footer todos={todos} checkAllTodo={this.checkAllTodo} clearAllDone={this.clearAllDone} />
        </div>
      </div>
    )
  }
}