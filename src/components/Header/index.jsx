import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {nanoid} from 'nanoid';
import './index.css';

export default class Header extends Component {
    // Restrict props types
    static propTypes = {
        addTodo: PropTypes.func.isRequired
    }

    handleKeyUp = (event) => {
        const {keyCode, target} = event;
        if(keyCode !== 13) return
        // todo object can not be empty
        if(target.value.trim() === '') {
            alert('Input can not be empty')
            return;
        }
        // Create a new todo object
        const todoObj = {id: nanoid(), name:target.value, done: false}
        // Pass the todo object to App object
        this.props.addTodo(todoObj)
        // Clear the input value
        target.value = '';
    }
    render() {
        return (
            <div className="todo-header">
               <input onKeyUp={this.handleKeyUp} type="text" placeholder="Please enter your task name and press enter to confirm" />
            </div>
        )
    }
}
